# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Requirements on system ###

* Have memcached installed
* Have nodejs installed
* configure enviroment

* PORT=3001
* MEMCACHED=localhost:11211
* LANGUAGES=en-gb,de-de
* BASE_PATH=https://partners.betvictor.mobi/%s/in-play/1/events
* REFRESH_RATE='*/3 * * * * *'



### Start ###

* run npm install
* npm run test 
* npm run start

### Routes ###

* host:PORT//en-gb/sports => list all sports
* host:PORT//en-gb/sports/{id} => list all competitions for sport id
* host:PORT//en-gb/events => list all events
* host:PORT//en-gb/events/{id} => get all info for event id
