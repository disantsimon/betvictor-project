import 'mocha'
var assert = require('chai').assert;
const config = require('../src/business/services/config.service');

describe('Config service', function() {

    context('Should created', function() {
        it('Should not be null', function() {
            assert.notEqual(config, null);
        })
    });
    context('Should have values', function() {
        it('PORT', function() {
            assert.notEqual(config.PORT, undefined);
        });
        it('MEMCACHED', function() {
            assert.notEqual(config.MEMCACHED, undefined);
        });
        it('LANGUAGES', function() {
            assert.notEqual(config.LANGUAGES, undefined);
        });
        it('REFRESH_RATE', function() {
            assert.notEqual(config.REFRESH_RATE, undefined);
        });
        it('PATHS', function() {
            assert.notEqual(config.PATHS, undefined);
        })
    })
});
