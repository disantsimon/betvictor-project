import 'mocha'

var assert = require('chai').assert;

import {LiveUpdate} from '../src/business/services/live-update'
import {MemcacheStoreService} from "../src/business/services/memcache-store.service";
import {MemcacheReadService} from "../src/business/services/memcache-read.service";

const config = require('../src/business/services/config.service');

const live = new LiveUpdate(config.PATHS, new MemcacheStoreService(config.MEMCACHED));
const read = new MemcacheReadService(config.MEMCACHED);

describe('LiveUpdateService', async function () {

    context('Should created', function () {
        it('Should not be null', function () {
            assert.notEqual(live, null);
        })
    });

    let allEvents: string | any[] = [];
    let sports: string | any[] = [];
    let event: any = {};
    let notFound: any = undefined;
    let allLanguageSports: any = {};
    let randomId = 0;
    beforeEach(async () => {
        await live.load();

        allEvents = await read.getAll(config.LANGUAGES[0]);

        sports = await read.getSports(config.LANGUAGES[0]);

        event = await read.getEvent(allEvents[0].id, config.LANGUAGES[0]);

        randomId = Math.floor(Math.random() * allEvents.length * 1000);
        notFound = await read.getEvent(randomId, config.LANGUAGES[0]);

        for (let lang of config.LANGUAGES) {
            allLanguageSports[lang] = (await read.getSports(lang))
        }
    });


    describe('Memcached should be ready', async function () {

        it(`check event for language ${config.LANGUAGES[0]} `, function () {
            assert.isNotEmpty(allEvents, `getAll returned undefined`)
        });

        it(`check sports for language ${config.LANGUAGES[0]}`, function () {
            assert.isNotNull(sports, `getSports returned undefined`)
        });
        it(`Check event by given id is not null`, function () {
            assert.isNotNull(event, `getEvent returned undefined`)
        });

        it(`Random id produce undefined`, function () {
            assert.isUndefined(notFound, `random id should be undefined`)
        });

        it(`check if all languages have been loaded`, function () {
            assert.isTrue(Object.values(allLanguageSports).filter(i => i).length === config.LANGUAGES.length, `No event founded`)
        })
    })
});

