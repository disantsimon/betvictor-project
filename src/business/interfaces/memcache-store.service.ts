import {IEvent, ISport} from "../../models/response";
import {SportDto} from "../../models/sport.dto";

export interface IMemcacheStoreService {
    test(): Promise<any | boolean>

    storeSports(arr: SportDto[], languageId: string): Promise<any>

    saveCompetitions(sport: ISport, languageId: string): Promise<any>;

    storeEvent(event: IEvent, languageId: string): Promise<any>;

    storeAll(event: IEvent[], languageId: string): Promise<any>;
}
