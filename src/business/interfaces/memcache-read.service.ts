import {Comp, IEvent} from "../../models/response";
import {SportDto} from "../../models/sport.dto";

export interface IMemcacheReadService {
    test(): Promise<any | boolean>

    getSports(languageId: string): Promise<SportDto[]>;

    getEvent(id: number, languageId: string): Promise<IEvent>;

    getCompetitions(sportId: number, languageId: string): Promise<Comp[]>

    getAll(languageId: string): Promise<Comp[]>
}
