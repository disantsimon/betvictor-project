export interface IConfigService {
    PORT: number | undefined;
    MEMCACHED: string | undefined;
    LANGUAGES: string[] | undefined;
    PATHS: { [lang: string]: string } | undefined;
    REFRESH_RATE: string | undefined;
}
