import {IMemcacheStoreService} from "../interfaces/memcache-store.service";
import { IEvent,  ISport} from "../../models/response";
import Memcached from 'memcached'

export class MemcacheStoreService implements IMemcacheStoreService {
    private memcached: Memcached;

    constructor(host: string) {
        this.memcached = new Memcached(host, {timeout: 2000});
    }

    test(): Promise<any | boolean> {
        return new Promise((resolve, reject) => {
            this.memcached.set('__MEMCACHED_TEST__', 'test', 10, (err) => {
                if (err) {
                    reject(err);
                    this.memcached.end();
                } else {
                    resolve(true)
                }
            })
        })
    }

    storeSports(arr: [], languageId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.memcached.set(`SPORTS_${languageId}`, arr, 10, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            });
        })
    }

    saveCompetitions(sport: ISport, languageId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.memcached.set(`SPORT_${languageId}_${sport.id}`, sport.comp, 10, (err: any) => {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    }

    storeEvent(event: IEvent, languageId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.memcached.set(`EVENT_${languageId}_${event.id}`, event, 10, (err: any) => {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    }

    storeAll(events: IEvent[], languageId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.memcached.set(`ALL_EVENTS_${languageId}`, events, 10, (err: any) => {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    }

}
