var fs = require('fs');
var util = require('util');
var logFile = fs.createWriteStream('log.txt', {flags: 'a'});

class Logger {
    private levels: { [lvl: number]: string } = {
        1: 'info',
        2: 'warn',
        3: 'verbose',
        4: 'error',
        5: 'fatal'
    }

    log(info: any) {
        logFile.write(this.message(1, JSON.stringify(info)));
    }

    error({args}: { args?: any[] }) {
        logFile.write(this.message(5, util.format.apply(null, arguments)));
    }

    warn(warn: any) {
        logFile.write(this.message(3, JSON.stringify(warn)));
    }

    fatal({args}: { args?: any[] }) {
        logFile.write(this.message(5, util.format.apply(null, arguments)));
    }

    private message(lvl: number, message: string) {
        const msg = `[${this.levels[lvl]}] ${new Date().toISOString()} \n ${message} \n` +
        `<---------------------------------------------------------------------> \n `;
        return msg;
    }


}

module.exports = new Logger();
