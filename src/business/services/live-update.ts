import {IEvent, ILiveResponse, ISport} from '../../models/response'
import {IMemcacheStoreService} from "../interfaces/memcache-store.service";
import {SportDto} from "../../models/sport.dto";

const axios = require('axios');

type paths = { [language: string]: string };

export class LiveUpdate {

    constructor(private paths: paths, private memcacheService: IMemcacheStoreService) {
    }

    path(id: string) {
        if (this.paths[id])
            return this.paths[id];

        throw {name: 100000, message: `${id} language unavailable`}
    }

    async load() {
        for (const key of Object.keys(this.paths)) {
            await this.liveApi(key);
        }
    }



    async liveApi(languageId: string) {
        const result = await axios.get(this.path(languageId));

        if (result.status.success === false) {
            throw result.status
        }

        const data: ILiveResponse = result.data;
        const sports = data.result.sports;

        const _sports = sports.map(value => {
            return new SportDto(value.id, value.desc, value.pos)
        }).sort((s1: any, s2: any) => s1.pos - s2.pos);

        await this.memcacheService.storeSports(_sports, languageId);

        const events = sports.reduce(
            (arr, sport) => {
                const tmp: IEvent[] = sport.comp.reduce((events, comp) => {
                    events = events.concat(comp.events);
                    return events;
                }, [] as IEvent[]);
                arr = arr.concat(tmp);
                return arr;
            }, [] as IEvent[]
        );

        await this.memcacheService.storeAll(events, languageId);

        for (const sport of sports) {
            await this.memcacheService.saveCompetitions(sport, languageId)
        }

        const all = [];

        for (const event of events) {
            all.push(new Promise(async (resolve, reject) => {
                const result = await axios.get(this.paths[languageId] + `/${event.id}`);
                const $event: IEvent = result.data.result;
                await this.memcacheService.storeEvent($event, languageId);
                resolve(result)
            }))
        }

        return Promise.all(all);
    }

}


