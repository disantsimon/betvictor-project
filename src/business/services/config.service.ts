import {IConfigService} from "../interfaces/config.service";
import {sprintf} from "sprintf-js";

const env = require('dotenv');
env.config();

export class ConfigService implements IConfigService {
    LANGUAGES: string[] | undefined;
    MEMCACHED: string | undefined;
    PATHS: { [lang: string]: string } | undefined;
    PORT: number | undefined;
    REFRESH_RATE: string | undefined;

    constructor() {
        const langs = (process.env.LANGUAGES || '').split(',');
        this.LANGUAGES = langs;

        this.MEMCACHED = process.env.MEMCACHED;

        this.PATHS = (langs || []).reduce((res: { [lang: string]: string }, lang) => {
            res[lang] = sprintf(process.env.BASE_PATH || '', lang);
            return res;
        }, {});

        this.PORT = Number(process.env.PORT);
        this.REFRESH_RATE = process.env.REFRESH_RATE;
    }
}

module.exports = new ConfigService();
