import {IMemcacheReadService} from "../interfaces/memcache-read.service";
import {Comp, IEvent} from "../../models/response";
import Memcached from 'memcached'
import {SportDto} from "../../models/sport.dto";

export class MemcacheReadService implements IMemcacheReadService {
    private memcached: Memcached;

    constructor(host: string) {
        this.memcached = new Memcached(host);
    }

    test(): Promise<any | boolean> {
        return new Promise((resolve, reject) => {
            this.memcached.set('__MEMCACHED_TEST__', 'test', 10, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true)
                }
            })
        })
    }

    getSports(languageId: string): Promise<SportDto[]> {
        return new Promise((resolve, reject) => {
            this.memcached.get(`SPORTS_${languageId}`, (err: any, value: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(value)
                }
            })
        })
    }

    getCompetitions(number: number, languageId: string): Promise<Comp[]> {
        return new Promise((resolve, reject) => {
            this.memcached.get(`SPORT_${languageId}_${number}`, (err: any, value: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(value);
                }
            })
        })

    }

    getEvent(id: number, languageId: string): Promise<IEvent> {
        return new Promise((resolve, reject) => {
            this.memcached.get(`EVENT_${languageId}_${id}`, (err: any, value: any) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(value)
                }
            })
        })
    }

    getAll(languageId: string): Promise<Comp[]> {
        return new Promise((resolve, reject) => {
            this.memcached.get(`ALL_EVENTS_${languageId}`, (err: any, value: any) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(value)
                }
            })
        })
    }

}
