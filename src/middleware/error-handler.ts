import {NextFunction, Request, Response} from "express";
const logger = require('../business/services/logger');

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
    logger.error(err);
    res.status(err.code).send(err);
};
