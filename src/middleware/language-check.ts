import {NextFunction, Request, Response} from "express";
import {BadRequest} from "../exceptions/bad-request";
const configService = require('../business/services/config.service');
const logger = require('../business/services/logger');

export const languageCheck = (request: Request, res: Response, next: NextFunction) => {
    if (configService.LANGUAGES.indexOf(request.params.language) === -1) {
        const exc = new BadRequest(`language ${request.params.language} is not available`);
        logger.error(exc);
        throw exc;
    } else {
        next();
    }
};
