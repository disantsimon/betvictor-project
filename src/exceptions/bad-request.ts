export class BadRequest extends Error {
    public code = 400;

    constructor(public message: string) {
        super();
    }
}
