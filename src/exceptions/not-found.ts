
export class NotFound extends Error {
    public code = 404;

    constructor(public message: string) {
        super();
    }
}
