export interface ILiveResponse {
    status: Status;
    result: Result;
}

export interface Result {
    sports:                 ISport[];
    transitions_pgate_path: string;
    total_number_of_events: number;
}

export interface ISport {
    id:                number;
    epId:              number;
    desc:              string;
    pos:               number;
    ne:                number;
    eic:               number;
    v:                 boolean;
    mc:                boolean;
    ncmc:              number;
    nemc:              number;
    hasInplayEvents:   boolean;
    hasUpcomingEvents: boolean;
    marketTypes:       MarketType[];
    comp:              Comp[];
    streamingVisible:  boolean;
}

export interface Comp {
    id:     number;
    desc:   string;
    pos:    number;
    events: IEvent[];
}

export interface IEvent {
    id:            number;
    event_type:    EventType;
    event_path_id: number;
    sport_id:      number;
    desc:          string;
    oppADesc:      string;
    oppAId:        number;
    oppBDesc:      string;
    oppBId:        number;
    american:      null;
    inPlay:        boolean;
    time:          number;
    pos:           number;
    has_stream:    boolean;
    markets:       Market[];
    scoreboard:    Scoreboard;
}

export enum EventType {
    GameEvent = "GAME_EVENT",
}

export interface Market {
    id:      number;
    st:      number;
    pltNP:   number;
    ca:      boolean;
    next:    boolean;
    ew:      boolean;
    o:       O[];
    status:  number;
    current: boolean;
    des:     string;
    mbl:     number;
    mtId:    number;
    mtid:    number;
    eId:     number;
    pId:     number;
    pid:     number;
    prdDsc:  P;
    pltD:    number;
    pltDes:  PLTDES;
    p:       P;
}

export interface O {
    rcNum:  number;
    pr:     number;
    price:  Price;
    id:     number;
    des:    string;
    hidden: boolean;
    prId:   number;
    lineId: number;
    shD:    string;
    wdrn:   boolean;
    prF:    string;
    op:     number;
}

export interface Price {
    oid:       number;
    prid:      number;
    pr:        string;
    prd:       number;
    fdp:       string;
    h:         boolean;
    ms:        number;
    os:        number;
    timestamp: number;
}

export enum P {
    Match = "Match",
    The90Mins = "90 Mins",
}

export enum PLTDES {
    WinOnly = "Win only",
}

export interface Scoreboard {
    addresses:               Addresses;
    clockInSeconds?:         number;
    validAt?:                number;
    reversedClock?:          boolean;
    periodKey:               string;
    clockStatus:             ClockStatus;
    marketSuspensionReason?: MarketSuspensionReason;
    inPlay:                  boolean;
    redCardA?:               number;
    redCardB?:               number;
    stoppageTime?:           string;
    matchLength?:            number;
    eId:                     number;
    sId:                     number;
    clk:                     string;
    run:                     boolean;
    dsc:                     string;
    code:                    number;
    sTs:                     number;
    cal:                     boolean;
    act:                     number;
    oaId:                    number;
    obId:                    number;
    scr?:                    string;
    scrA?:                   number | string;
    scrB?:                   number | string;
    pId?:                    number;
    bos?:                    number;
    bog?:                    number;
    setA?:                   number;
    setB?:                   number;
    legA?:                   number;
    legB?:                   number;
    pointA?:                 number;
    pointB?:                 number;
    tiebreak1A?:             number;
    tiebreak1B?:             number;
    surface?:                string;
    gDsc?:                   string;
    "1s"?:                   string;
    "2s"?:                   string;
    "3s"?:                   string;
    "4s"?:                   string;
    "5s"?:                   string;
    "1sA"?:                  number;
    "1sB"?:                  number;
    "2sA"?:                  number;
    "2sB"?:                  number;
    "3sA"?:                  number;
    "3sB"?:                  number;
    "4sA"?:                  number;
    "4sB"?:                  number;
    "5sA"?:                  number;
    "5sB"?:                  number;
    pts?:                    string;
    ptsA?:                   string;
    ptsB?:                   string;
    setNum?:                 number;
    setScr?:                 string;
    totalPeriods?:           number;
    lastActivePeriod?:       number;
    "1qA"?:                  number;
    "1qB"?:                  number;
    "2qA"?:                  number;
    "2qB"?:                  number;
    "3qA"?:                  number;
    "3qB"?:                  number;
    "4qA"?:                  number;
    "4qB"?:                  number;
    otA?:                    number;
    otB?:                    number;
    "1hA"?:                  number;
    "1hB"?:                  number;
    "2hA"?:                  number;
    "2hB"?:                  number;
    "6sA"?:                  number;
    "6sB"?:                  number;
    "7sA"?:                  number;
    "7sB"?:                  number;
    "8sA"?:                  number;
    "8sB"?:                  number;
    "9sA"?:                  number;
    "9sB"?:                  number;
    "10sA"?:                 number;
    "10sB"?:                 number;
    "11sA"?:                 number;
    "11sB"?:                 number;
    "1pA"?:                  number;
    "1pB"?:                  number;
    "2pA"?:                  number;
    "2pB"?:                  number;
    "3pA"?:                  number;
    "3pB"?:                  number;
    penA?:                   number;
    penB?:                   number;
    "1IA"?:                  string;
    "1IB"?:                  string;
    Inum?:                   number;
}

export interface Addresses {
    comment:                     string;
    essentialScoreboard:         string;
    essentialScoreboardCallback: string;
    stats:                       string;
    timeline:                    string;
    overviewComment:             string;
}

export enum ClockStatus {
    EndOfEvent = "END_OF_EVENT",
    Started = "STARTED",
}

export enum MarketSuspensionReason {
    Empty = "",
    PossiblePenalty = "POSSIBLE_PENALTY",
}

export interface MarketType {
    mtId:        number;
    pos:         number;
    desc:        string;
    mtDesc:      string;
    coupon_name: string;
    headers:     Array<number | string>;
    periods:     Period[];
    pId:         number;
    pDesc:       string;
    sport_id:    number;
}

export interface Period {
    desc:      string;
    long_desc: string;
    pIds:      number[];
    config:    Config;
}

export interface Config {
    filter: string;
}

export interface Status {
    success:   boolean;
    errorCode: number;
    extraInfo: ExtraInfo;
}

export interface ExtraInfo {
}
