export class SportDto {
    constructor(public id: number,
                public desc: string,
                public pos: number) {
    }
}
