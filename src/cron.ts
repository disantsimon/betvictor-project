import {MemcacheStoreService} from "./business/services/memcache-store.service";
import {LiveUpdate} from "./business/services/live-update";

const cron = require("node-cron");
const logger = require('./business/services/logger')
const configService = require('./business/services/config.service');

const readService = new MemcacheStoreService(configService.MEMCACHED);

const liveUpdate = new LiveUpdate(configService.PATHS, readService);


readService.test().then(
    async () => {
        await liveUpdate.load();

        console.log('memcached listen on host: ', configService.MEMCACHED);
        cron.schedule(configService.REFRESH_RATE, async function () {
            await liveUpdate.load();
            console.log("reloaded data");
        });
    }
).catch(
    err => {
        logger.fatal(err);
        process.abort();
    }
);



