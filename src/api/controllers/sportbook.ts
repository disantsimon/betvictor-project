import {IMemcacheReadService} from "../../business/interfaces/memcache-read.service";
import {NotFound} from "../../exceptions/not-found";
import {NextFunction, Request, Response} from "express";


export class Sportbook {

    constructor(public memcacheRead: IMemcacheReadService) {
    }

    events(request: Request, response: Response, next: NextFunction) {


        this.memcacheRead.getCompetitions(parseInt(request.params.id), request.params.language).then(
            value => {
                if (!value) {
                    next(new NotFound(`${request.params.id} not found`));
                } else {
                    response.status(200).json(value)
                }
            }
        ).catch(
            err => {
                response.status(500).json(err)
            }
        )
    }

    sports(request: Request, response: Response) {
        this.memcacheRead.getSports(request.params.language).then(
            value => {
                response.send(value)
            }
        )
    }

    event(request: Request, response: Response, next: NextFunction) {
        this.memcacheRead.getEvent(parseInt(request.params.id), request.params.language).then(
            value => {
                if (!value) {
                    next(new NotFound(`${request.params.id} not found`));
                } else {
                    response.status(200).json(value)
                }
            }
        )
    }

    getAll(request: Request, response: Response) {
        this.memcacheRead.getAll(request.params.language).then(
            value => {
                response.status(200).json(value)
            }
        )
    }
}
