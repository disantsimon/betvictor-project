import express = require('express');
import {Sportbook} from "./controllers/sportbook";
import {MemcacheReadService} from "../business/services/memcache-read.service";
import {NotFound} from "../exceptions/not-found";
import {languageCheck} from "../middleware/language-check";
import {errorHandler} from "../middleware/error-handler";

const configService = require('../business/services/config.service');
const logger = require('../business/services/logger');

const sportbook = new Sportbook(new MemcacheReadService(configService.MEMCACHED));

const app = express();

app.listen(configService.PORT, (err: any) => {
    if (err) {
        logger.error(err);
        process.abort();
    } else {
        console.log('webserver listen on port:', configService.PORT)
    }
});

app.get('/:language/sports', languageCheck, sportbook.sports.bind(sportbook), errorHandler);
app.get('/:language/sports/:id', languageCheck, sportbook.events.bind(sportbook), errorHandler);
app.get('/:language/events', languageCheck, sportbook.getAll.bind(sportbook), errorHandler);
app.get('/:language/events/:id', languageCheck, sportbook.event.bind(sportbook), errorHandler);

app.route("*").get((request, response) => {
    const exc = new NotFound(`${request.url} not found`);
    logger.error(exc);
    response.status(404).json(exc)
});
